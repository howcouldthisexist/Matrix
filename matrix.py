
import argparse
import sys
import traceback

# Parse Arguments of Command Line
parser = argparse.ArgumentParser('Matrix multiplier')
parser.add_argument('termsFile', help='File expressing column vector terms matrix')
parser.add_argument('coefficientsFile', help='File expressing coefficients matrix')
args = parser.parse_args()

# Generic Try Block
try:

##################################################################
# Open Files    
    
    print('Opening Files...')
    
    coefficientsFile = open(args.coefficientsFile, 'r')
    termsFile = open(args.termsFile, 'r')
    
    # Define Lists for File Values
    coefficientsMatrix = []
    termsMatrix = []
    

##################################################################
# Read from Coefficients File

    print('Reading Coefficients File ' + str(args.coefficientsFile) + '...')
    
    line = coefficientsFile.readline().strip('\n')  # Read Initial Line
    
    # Iterate Over Matrix Rows/File Lines
    while (line != ''):
        
        coefficientRow = []     # Define New List for Coefficients Row
        numbers = line.split(' ')   # Create List from Space-Delimited Line Elements
        
        # Append Values to Current Row
        for value in numbers:
            coefficientRow.append(int(value))
            
        coefficientsMatrix.append(coefficientRow)       # Append Row to Matrix
        line = coefficientsFile.readline().strip('\n')  # Read Next Line
        

##################################################################
# Read from Terms File

    print('Reading Terms File ' + str(args.termsFile) + '...')
    
    line = termsFile.readline().strip('\n')  # Read Initial Line
    
    # Iterate Over Matrix Rows/File Lines
    while (line != ''):
        
        termsRow = []   # Define New List for Termss Row
        numbers = line.split(' ')   # Create List from Space-Delimited Line Elements
        
        # Append Values to Current Row
        for value in numbers:
            termsRow.append(int(value))
            
        termsMatrix.append(termsRow)       # Append Row to Matrix
        line = termsFile.readline().strip('\n')  # Read Next Line
        
                
        
##################################################################
# Matrix Dimensions Check

    print('Checking for Consistent & Valid Matrix Dimensions...')
    
    # Iterate Over Coefficients Matrix Rows
    for row in coefficientsMatrix:
        
            # Length of Row Not Equal to Length of Terms Column
            if (len(row) != len(termsMatrix)):
                
                print('Inconsistent Matrix Dimensions')
                sys.exit(1)
                
            # Length of Row Not Equal to Other Row
            elif (len(row) != len(coefficientsMatrix[0])):
                
                print('Coefficients Matrix not Consistently Sized')
                sys.exit(1)
                
    # Iterate Over Terms Matrix Rows
    for row in termsMatrix:
        
        # Length of Row Not Equal to Other Row
        if (len(row) != len(termsMatrix[0])):
            
            print('Terms Matrix not Consistently Sized')
            sys.exit(1)
            
            
##################################################################
# Transpose Dictionary References
#
#     Change references from matrix[column][row] to
# matrix[row][column] for computational simplicity.

    termsColumn = []        # Define New Matrix, Replacing termsMatrix
    b = 0                   # termMatrix Row Index
    
    # Iterate Over a Columns in termsMatrix
    while (b < len(termsMatrix[0])):
        
        a = 0       # termsMatrix Column Index
        termsColumn = []   # Define New Column List for Terms
        
        # Iterate Over Rows in a Column of termsMatrix
        while (a < len(termsMatrix)):
            
            termsColumn.append(termsMatrix[a][b])   # Append Value to New List Column
            a += 1
            
        termsColumn.append(termsColumn)     # Append Row to Column
        b += 1
        
        
##################################################################
# Multiplication

    print('Multiplying...')
    
    resultsMatrix = []      # Define New List for Results
    
    # Iterate Over Rows in Coefficients Matrix
    for c_row in coefficientsMatrix:
        
        resultsRow = []     # Define New Row in Results Matrix
        
        # Iterate Over Columns in Terms Matrix
        for column in termsColumn:
            
            accumulator = 0     # Define Sum of Row Additions
            i = 0
            
            # Iterate Over Elements in Row of Coefficients Matrix
            while (i < len(c_row)):
                
                accumulator += column[i]*c_row[i]   # Multiply Elements and Add to Row Sum
                i += 1
                
            resultsRow.append(accumulator)  # Append Value to Row
            
        resultsMatrix.append(resultsRow)    # Append Row to Results
        
    
##################################################################
# Write to File

    print('Writing Output File out.txt...')
    
    outFile = open('out.txt', 'w')
    
    # Iterate Over Rows in Result
    for row in resultsMatrix:
        
        # Write Results Row to File
        for element in row:
            
            outFile.write(' ' + str(element))
            
        outFile.write('\n')
        
    outFile.close()
    

##################################################################
# Simple Exception Handling

except:
    
    print('Error Processing')
#    print(traceback.format_exc())
    
    
coefficientsFile.close()
termsFile.close()

print('Done.')
    
    